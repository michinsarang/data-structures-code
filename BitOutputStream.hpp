#ifndef BITOUTPUTSTREAM_HPP
#define BITOUTPUTSTREAM_HPP

/** Interface of BitOutputStream
 *  See: BitOutputStream.cpp
 *  @author David Welch
 *  cs100uax
 */

#include <iostream>
#include <fstream>
using namespace std;

/** A class BitOutputStream which writes bits to a buffer
 *  from a passed char.
 */

class BitOutputStream {
	private:
		char buffer;
		ostream& out;
		int nbits;
	public:
		BitOutputStream(ostream& os) : out(os) {
			buffer = nbits = 0;
		}
		void flush();
		/* takes a char corresponding with a 0 or 1 and manipulates buffer 
		 * in the nbit'th place 
		 */
		void writebits(char bit);
};


#endif // BITOUTPUTSTREAM_HPP
