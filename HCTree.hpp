#ifndef HCTREE_HPP
#define HCTREE_HPP

#include <queue>
#include <vector>
#include "HCNode.hpp"
#include "BitInputStream.hpp"
#include "BitOutputStream.hpp"

/** Interface of the HCTree class
 *  See: HCTree.cpp
 *  @author David Welch
 *  cs100uax
 */

using namespace std;

/** A 'function class' for use as the Compare class in a
 *  priority_queue<HCNode*>.
 *  For this to work, operator< must be defined to
 *  do the right thing on HCNodes.
 */
class HCNodePtrComp {
public:
    bool operator()(HCNode*& lhs, HCNode*& rhs) const {
        return *lhs < *rhs;
    }
};

/** A Huffman Code Tree class.
 *  Not very generic:  Use only if alphabet consists
 *  of unsigned chars.
 */
class HCTree {

private:
    HCNode* root;
    vector<HCNode*> leaves;
	string temp;	// to hold bit encode
	long bitswritten;	// holds number of total bits written to file for use to calculate the remainder

public:
    explicit HCTree() : root(0) {
        leaves = vector<HCNode*>(256, (HCNode*) 0);
		bitswritten = 0;
    }

    virtual ~HCTree() {
		if( root != NULL) { makeEmpty(root); }
	}

    /** Use the Huffman algorithm to build a Huffman coding trie.
     *  PRECONDITION: freqs is a vector of ints, such that freqs[i] is 
     *  the frequency of occurrence of byte i in the message.
     *  POSTCONDITION:  root points to the root of the trie,
     *  and leaves[i] points to the leaf node containing byte i.
     */
    void build(const vector<long>& freqs);

    /** Write to the given BitOutputStream
     *  the sequence of bits coding the given symbol.
     *  PRECONDITION: build() has been called, to create the coding
     *  tree, and initialize root pointer and leaves vector.
     */
    void encode(BitOutputStream& out, ifstream& in2);

    /** Passed an ifstream, ofstream, BitInputStream, and some int file information.
	 *  Decodes and writes the information to file.
     *  PRECONDITION: build() has been called, to create the coding
     *  tree, and initialize root pointer and leaves vector.
     */
    void decode(BitInputStream& input, int remainder, long filesize, ofstream& os, ifstream& in) const;
	
	/** Destructor */
	void makeEmpty(HCNode* node);
	
	/** for tree traversal */
	void revtraverse(HCNode* n);
	
    /** clears the temporary string that holds 0 and 1's from tree traversal */
    void clearstring();
	
	/** get the number of bits written.
	 *  PRECONDITION: encode has been called
	 */
	long getbitswritten();
};

#endif // HCTREE_HPP
