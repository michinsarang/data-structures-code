/** P3
 *	Implementation of compression program
 *	See: HCTree.hpp, HCNode.hpp, BitOutputStream.hpp
 *  @Author David Welch
 *	cs100uax
 */

#include <cstdlib>
#include <iostream>
#include <queue>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "BitOutputStream.hpp"
#include "HCNode.hpp"
#include "HCTree.hpp"

using namespace std;

/** This program compresses a file by calling it from the command line:
 *  compress <name of infile> <name of outfile>. It reads in byte by byte
 *  and converts them to their corresponding 8 bit representation. It then
 *  builds a huffman tree, reads in the data again, and outputs the new
 *  compressed data, along with a header to rebuild the tree, to the outfile.
 */
int main(int argc, char** argv) {
    
	// check for the correct # of arguments
	if (argc != 3) {
        cout<<"compress <file name> <output name>"<<endl; 
        return 0;
    }
	
	ifstream in(argv[1], ios::binary);	// set up ifstream
	
	if(!in.good()) {
		cout<<"No such file or incorrect file name"<<endl;
		return 0;
	}
	
	vector<long> freqs(256,0);	// vectors holding frequencies and symbols
	
	unsigned char c;	// for use in character count and encoding
	
	/* count the number of symbols in file */
	while(in.is_open() && in.good() && !in.eof()){
		
		c = in.get();
		if(in.good())
			freqs[c] = freqs[c]+1;	// increments count of char c at the integer value of c
		else
			break;
	}

	/* build tree */
	HCTree* hufftree = new HCTree();
	hufftree->build(freqs);
	
	/* Construct tree representation data and write this to the header of the
	   file. */
	   
	in.close();
	
	ifstream in2(argv[1], ios::binary);	// set up ifstream
	ofstream os(argv[2], ios::binary);	// set up ofstream
	BitOutputStream* output = new BitOutputStream(os);	// new bitstream
	
	struct stat filesize;	// to get file size
	stat(argv[1], &filesize);   // get the file size
	
	os<<endl;   // delimit header part 1
	os<<" "<<endl;  // put a place holder for the bitswritten
	
	/* start writing to header */
	os<<filesize.st_size;	// file size first
	os<<endl;   // delimit
	
	/* write in frequency header */
	for(int i=0;i<256;i++) {
		if(freqs[i] != 0) {
			os<<i<<" "<<freqs[i]<<" ";
		}
	}

	os<<endl;   // delimit
	
	/* Read in the data again and use the huffman tree to write the compressed
	   data to a file. */
	while(in2.good() && !in2.eof()) {
		hufftree->encode(*output, in2);
		hufftree->clearstring();
	}

	output->flush();	// flush extra bits
	
	os.seekp(0, ios_base::beg); // set iterator to beginning
	
	int bitswritten;
	bitswritten = (hufftree->getbitswritten() % 8);	// calculate remaining bits

	os<<bitswritten;    // write in bits written
	os.clear(); // clear stream

	in2.close();    // close everything
	os.close();

	/* cleanup */
	delete output;
	delete hufftree;
    return 0;
}


