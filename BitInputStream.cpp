#include "BitInputStream.hpp"

/** Implementation of the BitInputStream class
 *  See: BitInputStream.hpp
 *  @author David Welch
 *  cs100uax
 */

/** Passed a char reference and extracts bits from it.
 *  returns a pointer to a char array of eight 0's and 1's.
 */
 char* BitInputStream::readbits(char c) {
 
	this->buffer = c;	// put character in buffer
	
	/* iterate through bits and add them to the array */
	for(int i=7;i>=0;i--) {
		if (0 < (this->buffer & (1<<7-i)))
			bitarray[i] = '1';	// set i'th index equal to 1
		else 
			bitarray[i] = '0';	// set i'th index equal to 0
	}
	
	this->buffer = 0;	// reset buffer
	return bitarray;
}



