#ifndef BITINPUTSTREAM_HPP
#define BITINPUTSTREAM_HPP

/** Interface of BitInputStream
 *  See: BitInputStream.cpp
 *  @author David Welch
 *  cs100uax
 */

#include <iostream>
#include <fstream>

using namespace std;

/** A class BitInputStream which reads in bits from a char
 *  and returns them in a char array.
 */
 
class BitInputStream {
	private:
		char buffer;
		istream& in;
		char bitarray[8];
	public:
		BitInputStream(std::istream& is) : in(is) { buffer = 0; }
		
		void fill();
		/* pass the character to read, returns a char array holding each value */
		char* readbits(char c);	
};

#endif // BITINPUTSTREAM_HPP
