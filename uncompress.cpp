/** P3
 *	Implementation of decompression program
 *	See: HCTree.hpp, HCNode.hpp, BitInputStream.hpp
 *  @Author David Welch
 *	cs100uax
 */
 
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>

#include "BitInputStream.hpp"
#include "HCNode.hpp"
#include "HCTree.hpp"

using namespace std;

/** This program reads in a file from the command line: 
 *  uncompress <name of infile> <name of outfile>, uncompresses the data
 *	and writes it to an outfile. It first reads in the header and reconstructs	
 *  the huffman tree. It then uses the tree to decode the information by
 *  traversing down from the root until it reaches a node containing a symbol.
 *  It then writes this to an outfile.
 */
int main(int argc, char** argv) {

	/* check for the correct # of arguments */
	if (argc != 3) {
        cout<<"compress <file name> <output name>"<<endl; 
        return 0;
    }
	
	ifstream in(argv[1], ios::binary);	// set up ifstream
	
	if(!in.good()) {
		cout<<"No such file or incorrect file name"<<endl;
		return 0;
	}
	
	string tempstring;	// used for string operations
	string rm;	// for use in bit remainder
	
	long filesize;	// store the integer file size
	int remainder;	// to store the integer representation of the remainder
	
	vector<long> freqs(256,0);	// vectors holding frequencies and symbols

	BitInputStream* input = new BitInputStream(in);	// new bitstream
	
	getline(in,rm);	// get bit remainder
	remainder = atoi(rm.c_str());	// convert it to an integer
	
	getline(in,tempstring);	// get the file size terminator
	filesize = atol(tempstring.c_str());	// convert it to a long int
	
	getline(in, tempstring);	// get the frequencies

	istringstream ss(tempstring);	// set up a string stream to seperate frequencies into vectors
    vector<long> items;
	copy(istream_iterator<long>(ss), istream_iterator<long>(), back_inserter<vector<long> >(items));

	for(int i=0;i<items.size();i = i+2) {	// seperate and store frequency values
		freqs[items[i]] = items[i+1];
	}

	ofstream os(argv[2], ios::binary);	// make a new file to write too
	
	/* reconstruct tree */
	HCTree* hufftree = new HCTree();	// make a new tree

	hufftree->build(freqs);	// rebuild the tree using freqs

	hufftree->decode(*input, remainder, filesize, os, in);	// decode everything

	/* close the streams */
	in.close();
	os.close();
    
	/* cleanup */
	delete hufftree;
	delete input;
	
    return 0;
}
