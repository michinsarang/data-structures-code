#include "BitOutputStream.hpp"

/** Implementation of BitOutputStream
 *  See: BitOutputStream.hpp
 *  @author David Welch
 *  cs100uax
 */

/* flush the buffer and resets it */
void BitOutputStream::flush() {
	out.put(buffer);
	out.flush();
	buffer = nbits = 0;
}

/* takes a char corresponding with a 0 or 1 and manipulates buffer 
 * in the nbit'th place 
 */
void BitOutputStream::writebits(char bit) {

	if(nbits == 8)
		this->flush();

	if(bit == '1') 
		buffer ^= (1<<7-nbits);
		
	nbits++;
}
