#include "HCTree.hpp"

/** Implementation of the HCTree class
 *  See: HCTree.hpp
 *  @author David Welch
 *  cs100uax
 */
 
/** This function uses the Huffman algorithm to build a Huffman coding trie.
 *  PRECONDITION: freqs is a vector of ints, where freqs[i] is the frequency
 *  of byte i in the message.
 *  POSTCONDITION:  root points to the root of the trie, and leaves[i] points 
 *  to the leaf node containing byte i.
 */
void HCTree::build(const vector<long>& freqs) {

	// initialize priority queue which will be used as the forest
    priority_queue<HCNode*,vector<HCNode*>,HCNodePtrComp> pq;
    
    HCNode* node;

	/* populate priority queue with new HCNodes initialized with frequency count
	and string symbol vector */
	for(int i=0;i<256;i++) {
		if(freqs[i] != 0) {	// we only want to populate nodes with more then 0 count
			node = new HCNode(freqs[i], i);	// make a new node with frequency and symbol
            leaves[i] = node;   // set the leaves[i] to point at node
			pq.push(node);	// put it in the pq
		}
	}
	
	/* While there are still more then one item in the queue, take the two 
	   highest priority nodes and construct them with a parent, updating the
	   parents count by the sum of the two node counts. Reinsert the parent 
	   of these nodes into the queue. Once there is one item in the queue
	   the tree is complete. */
	   
	int cmcount;	// to increment counts in parent node
	
	HCNode* parent;	// to instantiate new parent
	
	/* handle the special case where there is only one character */
	if(pq.size() == 1) {
		root = new HCNode();	// make root
		root->c0 = pq.top();
		pq.top()->ischild = 0;
		pq.top()->p = root;
		pq.pop();
	}
	
    /* construct tree from priority queue*/
	while(pq.size() > 1) {
	
		cmcount = 0;
	
		parent = new HCNode();  // make a new node

        /* attach the first lowest priority value */
		parent->c0 = pq.top();
		pq.top()->ischild = 0;
		pq.top()->p = parent;
		cmcount = pq.top()->count;
		pq.pop();
		
        /* attach the second lowest priority value */
		parent->c1 = pq.top();
		pq.top()->ischild = 1;
		pq.top()->p = parent;
		cmcount = cmcount + pq.top()->count;
		pq.pop();
		
		parent->count = cmcount;    // update counts for parent
		parent->symbol = parent->c0->symbol;	// sets symbol to break tiebreakers
		pq.push(parent);    // put it on the pq
	}
	if(pq.size() > 0)
		root = pq.top();	// set root
}

/** Writes to BitOutputStream the sequence of bits coding the given symbol.
 *  PRECONDITION: build() has been called, to create the coding tree, and
 *  initialize root pointer and leaves vector.
 */
void HCTree::encode(BitOutputStream& out, ifstream& in2) {
	
	unsigned char c;
    c = in2.get();
	if(in2.good()) {
		revtraverse(leaves[c]);    // 	get reverse traversal of symbol
		for(int i=0;i<temp.length();i++) {	// writes out the temp string to file
			out.writebits(temp[i]);
		}
	}
}

/** clear temp string before calculating a new leaf to root traversal */
void HCTree::clearstring() {
    this->temp = "";
}

/** Return symbol coded in the next sequence of bits from the stream.
 *  PRECONDITION: build() has been called, to create the coding
 *  tree, and initialize root pointer and leaves vector.
 */
void HCTree::decode(BitInputStream& input, int remainder, long filesize, ofstream& os, ifstream& in) const {
	
	HCNode* currnode;
	string treemove;	// string of sequence of 8 zero's and one's
	int lastbyte = 0;	// handle the last byte
	
	if(root != 0) {	// if the root is not null
	
		currnode = root;	// set the current node to root

		unsigned char c;	// hold the char

		int nbit = 8;	// used for early terminate on stray bits
			
		/* decode information */
		while(in.good()){
			c = in.get();
			if(in.good()) {	// make sure it's not EOF char
				treemove = input.readbits(c);	// read the bits into treemove string.
				if(filesize == lastbyte-1) {	// if its the last byte written modify
					nbit = nbit-remainder+1;	// nbit to change loop length.
				}
				for(int i=0;i<nbit;i++){		// loop through treemove		
					if(treemove[i] == '0') {	// if its a 0 go c0
						currnode = currnode->c0;
					}
					else {						// otherwise go c1
						currnode = currnode->c1;						
					}
					
					if(currnode->c0 == 0 && currnode->c1 == 0) {	// if we are now at a leaf
						if(filesize > lastbyte) {					// write the symbol of that leaf
							os.put(currnode->symbol);				// out to file.
							lastbyte++;								// increment the file size counter
						}
						currnode = root;							// reset the current node to root
					}
				}
			}
		}
	}
}

/** traverses from leaf to root, concatenating them to a string in reverse order */
void HCTree::revtraverse(HCNode* n) {
	
	while(n->p != 0) {	// while theres a parent i.e. not root
		if(n->ischild == 1) {	// if its a 1 child, add 1 to the front of the string
            temp.insert(0, "1");
			bitswritten++;		// increment bits written
		}
		else {					// else add 0
            temp.insert(0, "0");
			bitswritten++;	// increment bits written
		}
		n = n->p;	// set node pointer to the parent
	}
}

/** get the bits written during encode, for use in calculating remainder trash bits */
long HCTree::getbitswritten(){
	return this->bitswritten;
}

/** for use in destructor */
void HCTree::makeEmpty(HCNode* node) {
    if(node->c0 != NULL)
      makeEmpty(node->c0);
    if(node->c1 != NULL)
      makeEmpty(node->c1);
    delete node;
}
